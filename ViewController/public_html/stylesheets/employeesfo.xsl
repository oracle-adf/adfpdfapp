<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" exclude-result-prefixes="fo" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
   <xsl:output method="xml" version="1.0" omit-xml-declaration="no" indent="yes"/>
   <!--=========================-->
   <!--root element: Employees-->
   <!--=========================-->
   <xsl:template match="Employees">
      <fo:root>
         <fo:layout-master-set>
            <fo:simple-page-master master-name="simpleA4" page-height="29.7cm" page-width="21cm" margin-top="2cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
               <fo:region-body/>
            </fo:simple-page-master>
         </fo:layout-master-set>
         <fo:page-sequence master-reference="simpleA4">
            <fo:flow flow-name="xsl-region-body">
               <fo:block font-size="16pt" font-weight="bold" space-after="5mm">Employees</fo:block>
               <fo:block font-size="10pt">
                  <fo:table table-layout="fixed" width="100%" border-collapse="separate">
                     <fo:table-column column-width="4cm"/>
                     <fo:table-column column-width="4cm"/>
                     <fo:table-column column-width="4cm"/>
                     <fo:table-column column-width="4cm"/>
                     <fo:table-header text-align="center" background-color="silver" font-weight="bold">
                        <fo:table-row>
                           <fo:table-cell padding="1mm" border-style="solid">
                              <fo:block>ID</fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding="1mm" border-style="solid">
                              <fo:block>First Name</fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding="1mm" border-style="solid">
                              <fo:block>Last Name</fo:block>
                           </fo:table-cell>
                           <fo:table-cell padding="1mm" border-style="solid">
                              <fo:block>Job Id</fo:block>
                           </fo:table-cell>
                        </fo:table-row>
                     </fo:table-header>
                     <fo:table-body>
                        <xsl:apply-templates select="Employee"/>
                     </fo:table-body>
                  </fo:table>
               </fo:block>
            </fo:flow>
         </fo:page-sequence>
      </fo:root>
   </xsl:template>
   <!--=========================-->
   <!--child element: Employee-->
   <!--=========================-->
   <xsl:template match="Employee">
      <fo:table-row>
         <fo:table-cell>
            <fo:block>
               <xsl:value-of select="EmployeeId"/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block>
               <xsl:value-of select="FirstName"/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block>
               <xsl:value-of select="LastName"/>
            </fo:block>
         </fo:table-cell>
         <fo:table-cell>
            <fo:block>
               <xsl:value-of select="JobId"/>
            </fo:block>
         </fo:table-cell>
      </fo:table-row>
   </xsl:template>
</xsl:stylesheet>