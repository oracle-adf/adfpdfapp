package br.com.waslleysouza.view.beans;

import br.com.waslleysouza.model.EmployeesViewImpl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.Iterator;

import java.util.List;

import javax.faces.context.FacesContext;

import javax.servlet.ServletContext;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.sax.SAXResult;

import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.jbo.Key;

import oracle.jbo.RowSet;
import oracle.jbo.ViewObject;

import oracle.jbo.XMLInterface;

import oracle.xml.parser.v2.XMLNode;

import oracle.xml.pipeline.controller.Input;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.myfaces.trinidad.model.RowKeySet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class EmployeesBean {
    
    private static String TEMPLATE_PDF_EMPLOYEES = "stylesheets/employeesfo.xsl";

    private static FopFactory fopFactory = FopFactory.newInstance();
    
    private static TransformerFactory tFactory = TransformerFactory.newInstance();
        
    public EmployeesBean() {
    }
    
    private InputStream getTemplate(String template) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext().getResourceAsStream(template);
    }

    private void convertDOM2PDF(Node xml, InputStream template, OutputStream out) {
        try {
            // configure foUserAgent as desired
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

            try {
                // Construct fop with desired output format and output stream
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

                // Setup Identity Transformer
                TransformerFactory factory = TransformerFactory.newInstance();
                Transformer transformer = factory.newTransformer(new StreamSource(template));

                // Setup input for XSLT transformation
                Source src = new DOMSource(xml);

                // Resulting SAX events (the generated FO) must be piped through to FOP
                Result res = new SAXResult(fop.getDefaultHandler());

                // Start XSLT transformation and FOP processing
                transformer.transform(src, res);
            } finally {
                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    public void exportSelectedRowsToPDF(FacesContext facesContext, OutputStream outputStream) {
        RichTable table = (RichTable) JSFUtils.findComponentInRoot("t1");
        RowKeySet rksSelectedRows = table.getSelectedRowKeys();
        Iterator itrSelectedRows = rksSelectedRows.iterator();

        StringBuilder id = new StringBuilder("");
        while (itrSelectedRows.hasNext()) {
            Key key = (Key) ((List) itrSelectedRows.next()).get(0);
            id.append(key.getKeyValues()[0]);

            if (itrSelectedRows.hasNext()) {
                id.append(",");
            }
        }

        DCIteratorBinding dcIteratorBindings = ADFUtils.findIterator("EmployeesView1Iterator");
        ViewObject vo = dcIteratorBindings.getViewObject();
        RowSet rs = ((EmployeesViewImpl) vo).filterById(id.toString());

        try {
            convertDOM2PDF(rs.writeXML(0, XMLInterface.XML_OPT_ALL_ROWS), getTemplate(TEMPLATE_PDF_EMPLOYEES), outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exportAllRowsToPDF(FacesContext facesContext, OutputStream outputStream) {
        DCIteratorBinding dcIteratorBindings = ADFUtils.findIterator("EmployeesView1Iterator");
        ViewObject vo = dcIteratorBindings.getViewObject();

        try {
            convertDOM2PDF(vo.writeXML(0, XMLInterface.XML_OPT_ALL_ROWS), getTemplate(TEMPLATE_PDF_EMPLOYEES), outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
